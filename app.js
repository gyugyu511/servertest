const net = require('net');

// 모듈로 분리한 설정 파일 불러오기
const config = require('./config');

// 모듈로 분리한 데이터베이스 파일 불러오기
const database = require('./database/database');

const client_datahandle = require('./datahandle/datahandle');

var clients = [];

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || config.server_port;

const server = net.createServer((cSocket) => {
  
    console.log('client connected');
 
    var id =  cSocket.remoteAddress +':'+ cSocket.remotePort;
    cSocket.id = id;
    clients.push(cSocket);
    
    cSocket.on('end',   client_end);
    cSocket.on('close', client_close); 
    cSocket.on('error', client_error);  
    cSocket.on('data',  clinet_data);
});

server.listen(port, () => {
    console.log('server bound %s : %d', getServerIp(), port);
    
    var ip_addr = process.env.OPENSHIFT_NODEJS_IP   || '127.0.0.1';
    
    console.log(ip_addr);
    
    // default to a 'localhost' configuration:
    var connection_string = '127.0.0.1:27017/te';
    // if OPENSHIFT env variables are present, use the available connection info:
    if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD){
      connection_string = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
      process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
      process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
      process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
      process.env.OPENSHIFT_APP_NAME;
    }
    
    console.log(connection_string);
    //DB 초기화.
    // database.init(server, config, function(){
    //    client_datahandle.init(server);    
    //});
    
    server.on('error', server_error);
    server.on('close', server_close);
});
 
// ====================== CLINET =====================================

var client_end = function(){
    console.log('client_end');
    clients.splice(clients.indexOf(this), 1);
}

var client_close = function()
{
     clients.splice(clients.indexOf(this), 1);
    
    console.log('client_close: ' + this.remoteAddress +' '+this.remotePort);
}

var client_error = function(err){
    console.log('!!--- client_error ---!!');
    console.log(err.stack);
}

var clinet_data = function(data){
    
    client_datahandle.main(this, data);
     //console.log('data: ' + c.remoteAddress +' '+ c.remotePort);
    //console.log('data bound %s', data);
    //var jsonObj = JSON.parse(data);
    //console.log(jsonObj.name);
    //c.write(data);

    for(var i = 0 ; i < clients.length ; i++)
    {
        //console.log(clients[i].id);
    }

    clients.forEach(function(v){

        if(v != this)
        {
            console.log(v.id);
            //v.write(this.id+':'+data);
        }
    });
}

//============================== SERVER ========================================



var server_error = function(err){
    console.log('!!--- server_error ---!!');
    console.log(err.stack);
    throw err;
}

var server_close = function(){ 
    console.log('server_close');
    
    if (database.db) {
		database.db.close();
	}
}

// 프로세스 종료 시에 데이터베이스 연결 해제
process.on('SIGTERM', function () {
    console.log("프로세스가 종료됩니다.");
    server.close();
});

server.on('connection', function(socket)
{
    console.log('CONNECT: ' + socket.remoteAddress +':'+ socket.remotePort); 
});

function getServerIp() {
    var os = require('os');
    var ifaces = os.networkInterfaces();
    var result = '';
    for (var dev in ifaces) {
        var alias = 0;
        ifaces[dev].forEach(function(details) {
            if (details.family == 'IPv4' && details.internal === false) {
                result = details.address;
                ++alias;
            }
        });
    }
    return result;
}



