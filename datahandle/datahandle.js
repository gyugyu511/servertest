var datahandle = {};

datahandle.init = function(server){
    console.log('데이터 핸들러 초기화.');
    datahandle.database = server.database;
}

datahandle.main = function(client, data){
    console.log('data %s', data);
    
    var jsonObj = JSON.parse(data);
    
    switch(jsonObj.Header){
        case REQ_LOGIN: login(client, jsonObj.id); break;
        case REQ_CREATE_USER: addUser(client, jsonObj.data.id); break;
    }
    
    //client.write(data);
}

var login = function(client, id){
    console.log('%s', id);
    
    datahandle.database.UserModel.findById(id, function(err, results) {
        if (err) {
            callback(err, null);
            return;
        }

        console.log('아이디 [%s]로 사용자 검색결과', id);
        console.dir(results);

        if (results.length > 0) {
            console.log('아이디와 일치하는 사용자 찾음.');
            SendMessage(client, RES_LOGIN_SUCCESS, null);
        }
        else{
            console.log('사용자 없음.');
            SendMessage(client, RES_LOGIN_FAIL, null);
        }
    });
}

var addUser = function(client, id){
    console.log('addUser 호출됨.');
	
	// UserModel 인스턴스 생성
	var user = new datahandle.database.UserModel({"id":id, "gold":100});

	// save()로 저장
	user.save(function(err) {
		if (err) {
			callback(err, null);
			return;
		}
		
	    console.log("사용자 데이터 추가함.");
	});
    
    SendMessage(client, RES_LOGIN_SUCCESS, null);
    
}
var SendMessage = function(client, header, data){
    var msg = {};
    msg.Header = header;
    msg.data = data;
    //console.dir(msg);
    var obj = JSON.stringify(msg)
    client.write(obj);
}

module.exports = datahandle;

var REQ_LOGIN = 'login';
var REQ_CREATE_USER = 'create_user';

var RES_LOGIN_SUCCESS   = 'login_success';
var RES_LOGIN_FAIL      = 'login_fail';