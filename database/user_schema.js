var Schema = {};

Schema.createSchema = function(mongoose){
    
    // 스키마 정의
    var UserSchema = mongoose.Schema({
       id: {type: String, required: true, unique: true, 'default':''},
        gold:{type: Number, 'default': 0},
        created_at: {type: Date, index: {unique: false}, 'default': Date.now},
	    updated_at: {type: Date, index: {unique: false}, 'default': Date.now}
    });
    
    // 스키마에 static 메소드 추가
	UserSchema.static('findById', function(id, callback) {
		return this.find({id:id}, callback);
	});
	
	UserSchema.static('findAll', function(callback) {
		return this.find({}, callback);
	});
	
	console.log('UserSchema 정의함.');

	return UserSchema;
};


module.exports = Schema;
